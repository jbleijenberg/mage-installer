<?php
/**
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Magento Installer
 * @package     Magento Installer
 * @author      Jeroen Bleijenberg <jeroen@jcode.nl>
 * 
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
namespace Jcode\MageInstaller;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Jcode\MageInstaller\Composer\MageInstaller;

class Plugin implements PluginInterface
{

    /**
     * @param Composer|Composer\Composer $composer
     * @param Composer\IO\IOInterface|IOInterface $io
     */
    public function activate(Composer $composer, IOInterface $io)
    {
        $installer = new MageInstaller($io, $composer, 'jcode-mage-package');

        $composer->getInstallationManager()->addInstaller($installer);
    }
}