<?php
/**
 * NOTICE OF LICENSE
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 * DISCLAIMER
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Magento Installer
 * @package     Magento Installer
 * @author      Jeroen Bleijenberg <jeroen@jcode.nl>
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */
namespace Jcode\MageInstaller\Composer;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;
use Composer\Repository\InstalledRepositoryInterface;
use Composer\IO\ConsoleIO;
use Composer\Composer;
use Composer\Util\Filesystem;

class MageInstaller extends LibraryInstaller
{

	/**
	 * Array containing files and folders that are excluded from copying
	 *
	 * @var array
	 */
	protected $excludedFromCopy = array();

	protected $extra = array();

	/**
	 * @var PackageInterface
	 */
	protected $package;

	/**
	 * Extend construct method to ensure composer.json, README.md and .gitignore files
	 * are always exluded from copying, because these are project specific files.
	 *
	 * @param \Composer\IO\ConsoleIO $io
	 * @param \Composer\Composer $composer
	 * @param string $type
	 * @param \Composer\Util\Filesystem $filesystem
	 */
	public function __construct(ConsoleIO $io, Composer $composer, $type = 'library', Filesystem $filesystem = null)
	{
		parent::__construct($io, $composer, $type, $filesystem);

		$this->excludedFromCopy = array(
			getcwd() . '/composer.json',
			getcwd() . '/README.md',
			getcwd() . '/.gitignore',
			getcwd() . '/.git',
		);

		$this->extra = $composer->getPackage()->getExtra();

	}

	public function getPackageBasePath(PackageInterface $package)
	{
		$prefix = substr($package->getPrettyName(), 0, 13);

		if ('jcode/mage-' !== $prefix) {
			throw new \InvalidArgumentException('Unable to installer package. Package name should always start with "jcode/mage-"');
		}

		return parent::getPackageBasePath($package);
	}

	/**
	 * On install make sure the module directories are empty by removing them.
	 * After removing, move all the module files
	 *
	 * @param InstalledRepositoryInterface $repo
	 * @param PackageInterface $package
	 */
	public function install(InstalledRepositoryInterface $repo, PackageInterface $package)
	{
		parent::install($repo, $package);

		if (array_key_exists('exclude-from-copy', $this->extra)) {
			foreach ($this->extra['exclude-from-copy'] as $exclude) {
				$this->excludedFromCopy[] = getcwd() . '/' . $exclude;
			}
		}

		$d = glob($this->getInstallPath($package) . '/app/code/*/*/*');

		foreach ($d as $dir) {
			$mageDir = getcwd() . str_replace($this->getInstallPath($package), '', $dir);

			$this->removeDir($mageDir);
		}

		$this->movePackageFiles($package);
	}

	/**
	 * Remove packages from Magento
	 *
	 * @param InstalledRepositoryInterface $repo
	 * @param PackageInterface $package
	 */
	public function uninstall(InstalledRepositoryInterface $repo, PackageInterface $package)
	{
		$d = glob($this->getInstallPath($package) . '/app/code/*/*/*');

		foreach ($d as $dir) {
			$mageDir = getcwd() . str_replace($this->getInstallPath($package), '', $dir);

			$this->removeDir($mageDir);
		}


		$f = glob($this->getInstallPath($package) . '/app/etc/modules/*.xml');

		foreach ($f as $file) {
			$mageFile = getcwd() . str_replace($this->getInstallPath($package), '', $file);

			if (file_exists($mageFile)) {
				unlink($mageFile);
			}
		}

		parent::uninstall($repo, $package);
	}

	/**
	 * On update, fully remove the modules from the file system to ensure that old files that are no longer used, are removed.
	 * After removing move the module files and directories from vendor/ to the right location within Magento
	 *
	 * @param InstalledRepositoryInterface $repo
	 * @param PackageInterface $initial
	 * @param PackageInterface $target
	 */
	public function update(InstalledRepositoryInterface $repo, PackageInterface $initial, PackageInterface $target)
	{
		parent::update($repo, $initial, $target);

		$filesToIgnore = array();

		if (array_key_exists('exclude-from-copy', $this->extra)) {
			foreach ($this->extra['exclude-from-copy'] as $exclude) {
				$this->excludedFromCopy[] = getcwd() . '/' . $exclude;
			}
		}

		$d = glob($this->getInstallPath($target) . '/app/code/*/*/*');

		foreach ($d as $dir) {
			$mageDir = getcwd() . str_replace($this->getInstallPath($target), '', $dir);

			$this->removeDir($mageDir);
		}

		$this->movePackageFiles($target, $filesToIgnore);
	}

	/**
	 * Recursivly remove a directory
	 * @param $dir
	 */
	protected function removeDir($dir)
	{
		if (is_dir($dir)) {
			$objects = scandir($dir);

			foreach ($objects as $object) {
				if ($object != '.' && $object != '..') {
					if (filetype($dir . '/' . $object) == 'dir') {
						$this->removeDir($dir . '/' . $object);
					} else {
						if ($dir && $object && ($dir . '/' . $object) != '/') {
							unlink($dir . '/' . $object);
						}
					}
				}
			}

			reset($objects);

			rmdir($dir);
		}
	}

	/**
	 * Move package files and directorties from /vendor to mage root.
	 *
	 * @param PackageInterface $package
	 */
	public function movePackageFiles(PackageInterface $package)
	{
		$this->package = $package;

		foreach (glob($this->getInstallPath($package)) as $source) {
			$destination = str_replace($this->getInstallPath($package) , getcwd(), $source);

			$this->copy($source, $destination);
		}

		return;
	}

	/**
	 * Copy a file or directory to the given destination.
	 * If the file is .htaccess, check if it already exists.
	 * If it does, leave it as is.
	 *
	 * @param $source
	 * @param $destination
	 *
	 * @return null
	 */
	public function copy($source, $destination)
	{
		if (!in_array($destination, $this->excludedFromCopy)) {
			$umask = umask(0);

			if (is_file($source)) {
				/**
				 * Check if the file may be overwritten or not.
				 */
				if ($this->isExcludedFromOverwrite($source, $destination)) {
					return null;
				}

				copy($source, $destination);
			} else {
				if (!is_dir($destination)) {
					mkdir($destination);
				}

				$dir = dir($source);

				while (false !== ($entry = $dir->read())) {
					if ($entry != '.' && $entry != '..') {
						$this->copy("{$source}/{$entry}", "{$destination}/{$entry}");
					}
				}

				$dir->close();

				return null;
			}

			umask($umask);
		}

		return null;
	}

	/**
	 * Check if a file is excluded from overwriting.
	 * If the file doesn't exist yet, copying it is fine. but if it exists leave it
	 *
	 * @param $source
	 * @param $destination
	 * @return bool
	 */
	protected function isExcludedFromOverwrite($source, $destination)
	{
		$baseFile = str_replace($this->getInstallPath($this->package) . DIRECTORY_SEPARATOR, '', $source);

		if (array_key_exists('prevent-overwrite', $this->extra)) {
			if (in_array($baseFile, $this->extra['prevent-overwrite']) && file_exists($destination)) {
				return true;
			}
		}

		return false;
	}
}